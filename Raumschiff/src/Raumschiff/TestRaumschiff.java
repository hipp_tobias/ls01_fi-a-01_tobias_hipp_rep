package Raumschiff;

public class TestRaumschiff {

	public static void main(String[] args) {

		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		Ladung ferengiSchneckensaft = new Ladung("Ferengi Schneckensaft", 200);
		Ladung klingonenSchwert = new Ladung("Bat'leth", 200);

		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		Ladung borgSchrott = new Ladung("Borg-Schrott", 5);
		Ladung roteMateria = new Ladung("Rote Materie", 2);
		Ladung plasmaWaffe = new Ladung("Plasma-Waffe", 50);

		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
		Ladung forschungssonde = new Ladung("Forschungssonde", 35);
		Ladung photonentorpedo = new Ladung("Photonentorpedo", 3);

		klingonen.addLadung(ferengiSchneckensaft);
		klingonen.addLadung(klingonenSchwert);

		romulaner.addLadung(borgSchrott);
		romulaner.addLadung(roteMateria);
		romulaner.addLadung(plasmaWaffe);

		vulkanier.addLadung(forschungssonde);
		vulkanier.addLadung(photonentorpedo);

		System.out.println("Klingonen: Photonentorpedos bereit. Abschuss auf die Romulaner!!!!! \n");
		System.out.println("peeeeennnggg!!!! \n");
		klingonen.photonentorpedoSchiessen(romulaner); // Klingonen schießen mit Photonentorpedo auf die Romulaner
		System.out.println("------------------------------");

		System.out.println("Romulaner: Phaserkanonen bereit. Abschuss auf die Klingonen!!!!! \n");
		System.out.println("peeeeennnggg!!!! \n");
		romulaner.phaserkanonenSchiessen(klingonen); // Romulaner schießen mit der Phaserkanone zurück
		System.out.println("------------------------------");

		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch"); // Nachricht an alle von den Vulkaniern, wird nicht
																// ausgegeben, da broadcast add ist

		klingonen.zustandRaumschiff(); // Klingonen rufen den Zustand des Raumschiffs ab
		klingonen.ladungsverzeichnisAusgeben(); // Klingonen geben ihr Ladungsverzeichnis aus

		vulkanier.reparaturDurchfuehren(true, true, true, 5); // Vulkanier setzen alle Androiden ein um das Schiff zu
																// reparieren
		System.out.println("------------------------------");
		System.out.println("Vulkanier: Androiden !! Reparatur durchführen! Sofort!!! \n");
		System.out.println("Androiden: Reparatur durchgeführt, Boss! Bibup..bibuu \n");
		System.out.println("Vulkanier: Schnell...Nachladen..!! \n");
		vulkanier.photonentorpedossLadung(3); // Vulkanier laden die Torpedoröhren des Rausmchiffes nach
		vulkanier.ladungsverzeichnisAufraeumen(); // Verzeichnis wird aufgeräumt
		System.out.println("Androiden: Photonentorpedos wurden nachgeladen, Boss! Bibup..bibuu");

		klingonen.photonentorpedoSchiessen(romulaner); // Es wird 2x auf die Romulaner geschossen
		klingonen.photonentorpedoSchiessen(romulaner);
		System.out.println("------------------------------");
		klingonen.zustandRaumschiff(); // Klingonen rufen den Zusatnd des Raumschiffes auf und geben ihr
										// Ladungsverzeichnis aus
		klingonen.ladungsverzeichnisAusgeben();
		System.out.println("------------------------------");
		romulaner.zustandRaumschiff();// Klingonen rufen den Zusatnd des Raumschiffes auf und geben ihr
										// Ladungsverzeichnis aus
		romulaner.ladungsverzeichnisAusgeben();
		System.out.println("------------------------------");
		vulkanier.zustandRaumschiff();// Klingonen rufen den Zusatnd des Raumschiffes auf und geben ihr
										// Ladungsverzeichnis aus
		vulkanier.ladungsverzeichnisAusgeben();
		System.out.println("------------------------------");

		System.out.println(Raumschiff.eintraegeLogbuchZurueckgeben());

	}

}
