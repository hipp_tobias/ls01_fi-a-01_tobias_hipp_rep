package Raumschiff;

/**
 * Diese Klasse modeliert die Klasse Ladung
 * 
 * @author Tobias Hipp
 * @version 1.4.1 vom 02.05.2021
 */

public class Ladung {
	private String bezeichnung;
	private int menge;

	/**
	 * Parameterloser Konstruktor der Klasse Raumschiff
	 */

	Ladung() { // Konstruktor (Methode1)

	}

	/**
	 * 
	 * @param bezeichnung, gibt die Bezeichnung der Klasse Ladung aus
	 * @param menge,       gibt die Menge der Klasse Ladung aus
	 */
	Ladung(String bezeichnung, int menge) { // Konstruktor (Methode2)
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	public String getBezeichnung() {// get
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {// set
		this.bezeichnung = bezeichnung;
	}

	public int getMenge() {// get
		return menge;
	}

	public void setMenge(int menge) {// set
		this.menge = menge;
	}
}
