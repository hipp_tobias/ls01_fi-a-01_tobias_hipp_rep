package Raumschiff;

import java.util.ArrayList;

/**
 * Diese Klasse modelliert ein Raumschiff
 * 
 * @author Tobias Hipp
 * @version 1.4.1 vom 02.05.2021
 */

public class Raumschiff {// Klasse

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int zustandSchildeInProzent;
	private int zustandHuelleInProzent;
	private int zustandLebenserhaltungssystemeInProzent;
	private int anzahlDroiden;
	private String schiffsname;
	private static ArrayList<String> broadcastkommunikator = new ArrayList<String>(); // nach new wird Konstruktor
																						// aufgerufen
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	/**
	 * Parameterloser Konstruktor der Klasse Raumschiff
	 */

	Raumschiff() {// Konstruktor (Mehtode1)

	}

	/**
	 * Vollparametrisierte Klasse Raumschiff
	 * 
	 * @param photonentorpedoAnzahl                   ist die Anzahl der vorhandenen
	 *                                                Photonentorpedos
	 * @param energieversorgungInProzent              ist die vorhandene prozentuale
	 *                                                Energieversorgung
	 * @param zustandSchildeInProzent                 ist der aktuelle Zustand der
	 *                                                Schilde
	 * @param zustandHuelleInProzent                  ist der aktuelle Zustand der
	 *                                                Huelle
	 * @param zustandLebenserhaltungssystemeInProzent ist der aktuelle Zustand der
	 *                                                Lebenserhaltungsysteme
	 * @param anzahlDroiden                           ist die vorhandenen Droiden
	 *                                                Anzahl
	 * @param schiffsname                             ist der Name des jeweiligen
	 *                                                Raumschiffs
	 */

	Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, // Konstruktor (Mehtode2)
			int zustandSchildeInProzent, int zustandHuelleInProzent, int zustandLebenserhaltungssystemeInProzent,
			int anzahlDroiden, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.zustandSchildeInProzent = zustandSchildeInProzent;
		this.zustandHuelleInProzent = zustandHuelleInProzent;
		this.zustandLebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
		this.anzahlDroiden = anzahlDroiden;
		this.schiffsname = schiffsname;

	}

	public int getPhotonentorpedoAnzahl() {// get
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {// set
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {// get
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {// set
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getZustandSchildeInProzent() {// get
		return zustandSchildeInProzent;
	}

	public void setZustandSchildeInProzent(int zustandSchildeInProzent) {// set
		this.zustandSchildeInProzent = zustandSchildeInProzent;
	}

	public int getZustandHuelleInProzent() {// get
		return zustandHuelleInProzent;
	}

	public void setZustandHuelleInProzent(int zustandHuelleInProzent) {// set
		this.zustandHuelleInProzent = zustandHuelleInProzent;
	}

	public int getZustandLebenserhaltungssystemeInProzent() {// get
		return zustandLebenserhaltungssystemeInProzent;
	}

	public void setZustandLebenserhaltungssystemeInProzent(int zustandLebenserhaltungssystemeInProzent) {// set
		this.zustandLebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
	}

	public int getAnzahlDroiden() {// get
		return anzahlDroiden;
	}

	public void setAnzahlDroiden(int anzahlDroiden) {// set
		this.anzahlDroiden = anzahlDroiden;
	}

	public String getSchiffsname() {// get
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {// set
		this.schiffsname = schiffsname;
	}

	/**
	 * Die Ladung von Klasse Ladung wird dem Ladungsverzeichnis hinzugef�gt
	 * 
	 * @param neueLadung wird ein Objekt der Klasse Ladung hinzugef�gt
	 */
	public void addLadung(Ladung neueLadung) {// add
		ladungsverzeichnis.add(neueLadung);
	}

	/**
	 * �ffentliche Methode mit Z�hlschleife und ohne R�ckgabewert
	 * 
	 * @param raumschiff; methode greift auf die Klasse Raumschiff zu. wenn die
	 *                    Photonentorpedo anzahl gleich 0 ist, dann wird eine
	 *                    Nachricht an alle versendet. Wenn PhotonentorpedoAnzahl
	 *                    abgeogen wird, dann wird eine nachtricht an alle
	 *                    versendet. Der Treffer wird vermerkt in dem der Name des
	 *                    Raumschiffes mit dem Treffer gespeichert wird.
	 */
	public void photonentorpedoSchiessen(Raumschiff raumschiff) {// Aufgabe 4
		if (photonentorpedoAnzahl == 0) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			photonentorpedoAnzahl--;
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			treffer(raumschiff);
		}
	}

	/**
	 * �ffentliche Methode mit Z�hlschleife und ohne R�ckgabewert
	 * 
	 * @param raumschiff; methode greift auf die Klasse Raumschiff zu. Wenn die
	 *                    energieversorgung kleiner als 50 ist, dann wird eine
	 *                    Nachricht an alle versandt. Wenn eine Phaserkanone
	 *                    abgeschossen wird dann geht die Energieversorgung um 50%
	 *                    runter. Treffer wird vermerkt, in dem der Name des
	 *                    Raumschiffs mit dem Treffer gespeichert wird.
	 */
	public void phaserkanonenSchiessen(Raumschiff raumschiff) {
		if (energieversorgungInProzent < 50) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			energieversorgungInProzent = energieversorgungInProzent - 50;
			nachrichtAnAlle("Phaserkanone abgeschossen");
			treffer(raumschiff);
		}
	}

	/**
	 * Private Methode mit Z�hlschleife und ohne R�ckgabewert
	 * 
	 * @param raumschiff; es wird ausgegeben, wenn ein Raumschiff getroffen wurde,
	 *                    dass der Raumschiffname mit dem Text getroffen ausgegeben
	 *                    wird und dann wird der wert vom zustandSchilde minus 50
	 *                    gesetzt/gespeichert. Wenn der Zustand der Schilde kleiner
	 *                    gleich 0 ist dann wird der Wert der Zustand Huelle sowie
	 *                    die Energieversorgung um minus 50 gesetzt. Wenn Zustand
	 *                    Huelle kleiner gleich 0 ist, dann wird der Wert der
	 *                    Lebenserhaltungssysteme auf 0 gesetzt. Und es wird eine
	 *                    Nachricht an alle versandt.
	 */
	private void treffer(Raumschiff raumschiff) {
		System.out.println(raumschiff.getSchiffsname() + " wurde getroffen!");
		raumschiff.setZustandSchildeInProzent(raumschiff.getZustandSchildeInProzent() - 50);
		if (getZustandSchildeInProzent() <= 0) {
			raumschiff.setZustandHuelleInProzent(raumschiff.getZustandHuelleInProzent() - 50);
			raumschiff.setEnergieversorgungInProzent(raumschiff.getEnergieversorgungInProzent() - 50);
			if (raumschiff.getZustandHuelleInProzent() <= 0) {
				raumschiff.setZustandLebenserhaltungssystemeInProzent(0);
				nachrichtAnAlle("Alle Lebenserhaltungssysteme wurden vernichtet!!!");
			}

		}
	}

	public void nachrichtAnAlle(String message) {
		broadcastkommunikator.add("\n" + this.schiffsname + ": " + message);
	}

	public static ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return broadcastkommunikator;

	}

	public void photonentorpedossLadung(int anzahlTorpedos) {

	}

	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int anzahlDroiden) {

	}

	/**
	 * Ausgabe des Zustandes der jeweiligen Raumschiffe
	 */
	public void zustandRaumschiff() {

		System.out.println("Schiffsname: " + schiffsname);
		System.out.println("Torpedo Anzahl: " + photonentorpedoAnzahl);
		System.out.println("Energie: " + energieversorgungInProzent);
		System.out.println("Huelle: " + zustandHuelleInProzent);
		System.out.println("Lebenserhaltungssystem: " + zustandLebenserhaltungssystemeInProzent);
		System.out.println("Schilde: " + zustandSchildeInProzent);
		System.out.println("Droiden: " + anzahlDroiden);

	}

	/**
	 * �ffentliche Methode mit Z�hlschleife und ohne R�ckgabewert
	 * 
	 * Das Ladungsverzeichnis wird gez�hlt, wenn i kleiner ist als die Gr��e des
	 * Ladungsverzechnis und wird dann plus 1 gez�hlt. Dann erfolgt die Ausgabe des
	 * Ladungsverzeichnis und ein Bezeichner und in der n�chsten Zeile das
	 * Ladungsverzeichnis mit der Menge.
	 */
	public void ladungsverzeichnisAusgeben() {
		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			System.out.println(ladungsverzeichnis.get(i).getBezeichnung());
			System.out.println(ladungsverzeichnis.get(i).getMenge());
		}
	}

	/**
	 * �ffentliche Methode mit Z�hlschleife und ohne R�ckgabewert
	 * 
	 * Das Ladungsverzeichnis wird gez�hlt, wenn i kleiner ist als die Gr��e des
	 * Ladungsverzechnis und wird dann plus 1 gez�hlt. Ausgegeben wird der
	 * Broadcastkommunikator.
	 */
	public void broadcastkommunikator() {
		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			System.out.println(broadcastkommunikator.get(i)); // ausgabe des Broadcast und mit i wird der index gew�hlt
			System.out.println(broadcastkommunikator.get(i));
		}
	}

	public void ladungsverzeichnisAufraeumen() {

	}

}
