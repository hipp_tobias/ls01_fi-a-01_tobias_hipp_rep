import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		
		// Eingabe
		String artikel = liesString("Sch�nen Guten Tag!, Was m�chten Sie bestellen?:");
		int anzahl = liesInt("Geben Sie bitte die Anzahl der Artikel ein:");
		double preis = liesDouble("Geben Sie bitte den Nettopreis des Artikel's ein:");
		double mwst = liesDouble("Geben Sie bitte den Mehrwertsteuersatz in Prozent ein:");
		
		//Verarbeitung
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
		
		//Ausgabe
		
		rechnungausgeben(artikel,  anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);


	}
//Methoden Anfang
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double nettogesamtpreis = anzahl * nettopreis;
		return nettogesamtpreis;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double bruttogesamtpreis;
		bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}
	
	public static String liesString(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		String artikel = myScanner.next();
		return artikel;
	}
	
	public static int liesInt(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		int anzahl = myScanner.nextInt();
		return anzahl;
	}
	
	public static double liesDouble(String text) { //MethodenKopf
		Scanner myScanner = new Scanner(System.in); //Methodenrumpf
		System.out.println(text);
		double zahl = myScanner.nextDouble();
		return zahl; // Zahl die ausgerechnet wurde, wird wieder ausgegeben!
	}
	
	public static void rechnungausgeben(String artikel, int anzahl, double
			nettogesamtpreis, double bruttogesamtpreis,
			double mwst) {
		
		System.out.println();
		System.out.println("Rechnung:");
		System.out.println("---------");
		System.out.printf("Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		System.out.println("------------------------------");
		System.out.println("Vielen Dank f�r Ihre Anfrage!");
	}

}