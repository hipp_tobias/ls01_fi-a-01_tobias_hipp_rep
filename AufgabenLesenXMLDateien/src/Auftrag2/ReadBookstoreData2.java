package Auftrag2;

import java.io.IOException;

import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

/* Arbeitsauftrag:  Lesen Sie den Titel und den Autor des Buches "Java ist auch eine Insel" 
*					 aus der Datei "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
*
*                   Ausgabe soll wie folgt aussehen:
*                        titel:  Java ist auch eine Insel 
*                        autor:  Max Mustermann 
*/

public class ReadBookstoreData2 {

	public static void main(String[] args) {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		try {

			// Name der Datei: "src/Auftrag3/buchhandlung.xml"
			// Add your code here
			DocumentBuilder dBuilder = factory.newDocumentBuilder();
			Document doc = dBuilder.parse("buchhandlung2.xml");
			NodeList titelList = doc.getElementsByTagName("titel");
			NodeList autorList = doc.getElementsByTagName("autor");
			Node titelNode = titelList.item(0);
			Node autorNode = autorList.item(0);
			System.out.println(titelNode.getNodeName() + ": " + titelNode.getTextContent());
			System.out.println(autorNode.getNodeName() + ": " + autorNode.getTextContent());

		} catch (Exception e) {
		}
	}
}