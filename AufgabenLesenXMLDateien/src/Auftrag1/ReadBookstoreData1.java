package Auftrag1;

import java.io.IOException;

import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class ReadBookstoreData1 {

	public static void main(String[] args) {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			// Name der Datei: "src/Auftrag1/buchhandlung.xml"
			// Add your code here

			DocumentBuilder dBuilder = factory.newDocumentBuilder();
			Document doc = dBuilder.parse("buchhandlung1.xml");
			NodeList titelList = doc.getElementsByTagName("titel");
			Node titelNode = titelList.item(0);
			System.out.println(titelNode.getNodeName() + ": " + titelNode.getTextContent());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
