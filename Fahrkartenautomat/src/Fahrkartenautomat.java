﻿import java.util.Scanner;

class Fahrkartenautomat{ //die Klasse für den Fahrkartenautomaten wird angelegt
	
    public static void main(String[] args){	// Methode wird erstellt
    	while (true) {

    	zuzahlenderbetrag = 0; //wird jeweils immer deklariert und auf "0" gesetzt
    	rückgabebetrag = 0;
    	eingezahlterGesamtbetrag = 0;
    	fahrkartenInfo(); // deklariert für den einen array
    	Scanner tastatur = new Scanner(System.in);  
    	zuzahlenderbetrag= fahrkartenBezahlung();

     
    	fahrkartenBestellung(); // deklariert für den einen array
    	fahrkartenAusgeben(); // deklariert für den einen array
    	rückgabebetrag = zuzahlenderbetrag *-1;
    	rückgeldAusgeben(rückgabebetrag);
    	} 
    }
    
    static int option; 
    static double eingezahlterGesamtbetrag;
    static double eingeworfeneMünze;;
    static double rückgabebetrag;
    static int anzahlkarten;
    static double zuzahlenderbetrag;
  
    
    public static void fahrkartenInfo() { // methode wird erstellt für die KartenInfo
    	
    	Scanner tastatur = new Scanner(System.in);  
    	
    	 // Array deklarieren für meine Karten Varrianten
        String[] karten = {
        		   "1. Einzelfahrschein Berlin AB: 2,90", //jede Varriante wird einzeln ausgegeben, damit man sie später im Array abgerufen werden kann
                   "2. Einzelfahrschein Berlin BC: 3,30",
                   "3. Einzelfahrschein Berlin ABC: 3,60",
                   "4. Kurzstrecke Berlin: 1,90",
                   "5. Tageskarte Berlin AB: 8,60",
                   "6. Tageskarte Berlin BC: 9,00",
                   "7. Tageskarte Berlin ABC: 9,60",
                   "8. Kleingruppen-Tageskarte Berlin AB: 23,50",
                   "9. Kleingruppen-Tageskarte Berlin BC: 24,30",
                  "10. Kleingruppen-Tageskarte Berlin ABC: 24,90" };
  
        System.out.println("Guten Tag, bitte wählen Sie eine der folgenden Fahrkarten:"); // Auswahl der Fahrkarten, mit der Eingabe von der jeweilig davostehenden Zahl   
        System.out.println("===================================="); // Trennung
            
         for(int i=0; i<karten.length; i++){ // Schleife beginnt, das der Array mit Option i gelesen wird
        System.out.println(karten[i]);  }    
        System.out.println("===================================="); //Trennung
        System.out.println("666. Bezahlen!"); // Nach der Auswahl den Varrianten und der jeweiligen Menge, muss man angeben ob man nun zum Bezahl Vorgang gehen möchte
    }
    
    public static double fahrkartenBezahlung() { // Methode wird erstellt, für die Bezahlung
    	
    	Scanner tastatur = new Scanner(System.in);  
    	double zuzahlenderbetrag=0;
    	option = 0 ;
    	while(option != 666) { //schleifen Ende
    	
        // Abfrage welche Option benötigt wird und wie viele
        System.out.println("====================================");
        System.out.println("Welche Fahrkarte benötigen Sie?: ");
        System.out.println("====================================");
        option = tastatur.nextInt(); // Eingabe vom Benutzer wird als option deklariert

    	// Kosten der Fahrscheine im Switch Case 
        // -------------------------------  	
        switch (option) { //Beginn des Switch-Case
    case 1: // Eingabe der beliebigen Zahl des Benutzers wird als "option" gelesen und hier im jeweiligen Case gesucht und ausgegeben.
       System.out.println(option);// ausgabe der eingabe des benutzers
       zuzahlenderbetrag += fahrkartenZählen()*2.90; // fahrkarte wird gezählt bzw der betrag, so oft der benutzer die karte halt haben möchte
        break; //pause
    case 2:
       System.out.println(option);
       zuzahlenderbetrag += fahrkartenZählen()*3.30;  
       	break;   
    case 3:
       System.out.println(option);
       zuzahlenderbetrag += fahrkartenZählen()*3.60; 
   		break;    
   	case 4:
       System.out.println(option);
       zuzahlenderbetrag += fahrkartenZählen()*1.90;
   		break;    
   	case 5:
       System.out.println(option);
       zuzahlenderbetrag += fahrkartenZählen()*8.60;
   		break;    
   	case 6:
       System.out.println(option);
       zuzahlenderbetrag += fahrkartenZählen()*9.00;
   		break;    
   	case 7:
       System.out.println(option);
       zuzahlenderbetrag += fahrkartenZählen()*9.60;
   		break;    
   	case 8:
       System.out.println(option);
       zuzahlenderbetrag += fahrkartenZählen()*23.50;
   		break;    
   	case 9:
       System.out.println(option);
       zuzahlenderbetrag += fahrkartenZählen()*24.30;
   		break;    
   	case 10:
       System.out.println(option);
       zuzahlenderbetrag += fahrkartenZählen()*24.90;
       	break;
   	default: // falls eine andere eingabe des benutzers kommt
   		if(option == 666) { // option bezahlen
   		}
   		else System.out.println("Dies ist eine ungültige Eingabe!");// falls eine andere Zahl eingegeben wird
   	break;}
        }
        return zuzahlenderbetrag; // wieder zurück kehren zu zuzahlenderBetrag
        }
    
    public static void rückgeldAusgeben(double rückgabebetrag) { // Methode wird erstellt, ausgeben des Rückgeldes
    	
        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
    	rückgabebetrag = Math.round(rückgabebetrag*100.0)/100.0; // Es wird der Rückgabebetrag ausgerechnet und die Kommastelle wird aufgerundet
        if(rückgabebetrag > 0.0) // zählschleife, solange der Rückgabebetrag größer als 0.0 ist, wird er unten ausgegeben
        {
     	   System.out.println("\nDer Rückgabebetrag in Höhe von, " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");
   	   
            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
            	münzeAusgeben(2,"Euro");
 	          rückgabebetrag = Math.round((rückgabebetrag - 2.0)*100)/100.00;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
            	münzeAusgeben(1,"Euro");
 	          rückgabebetrag = Math.round((rückgabebetrag - 1.0)*100)/100.00;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
            	münzeAusgeben(50,"Cent");
 	          rückgabebetrag = Math.round((rückgabebetrag - 0.50)*100)/100.00;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
            	münzeAusgeben(20,"Cent");
  	          rückgabebetrag = Math.round((rückgabebetrag - 0.20)*100)/100.00;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
            	münzeAusgeben(10,"Cent");
 	          rückgabebetrag = Math.round((rückgabebetrag - 0.10)*100)/100.00;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
            	münzeAusgeben(5,"Cent");
  	          rückgabebetrag = Math.round((rückgabebetrag - 0.05)*100)/100.00;
            }
        }
    }
    public static void fahrkartenBestellung() { //methode wird erstellt, für die Fahkarten bestellung
    	
    	Scanner tastatur = new Scanner(System.in);  
    	
        // Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0; // wird erstmal / wieder auf "0" gesetzt
        while(eingezahlterGesamtbetrag < zuzahlenderbetrag)
        {
     	   System.out.printf("Es sind: %.2f Euro zu zahlen!\n" , (zuzahlenderbetrag - eingezahlterGesamtbetrag)); // ausgabe und berechnung, was noch zu zahlen ist
     	   System.out.println("------------------------------"); // Trennung
     	   System.out.print("\nEinwurf mit: mind. 5Ct, höchstens 2 Euro:\n ");
     	   eingeworfeneMünze = tastatur.nextDouble(); // die eingeworfene münze wird gelesen
     	   zuzahlenderbetrag -= eingeworfeneMünze ; // der zuzahlende betrag wird minus die eingeworfene Münze gerechnet
        }
    }
    public static void fahrkartenAusgeben() { //Methode wird erstellt, womit die Fahkarten ausgabe bezeichnet wird
    	
    	 // Fahrscheinausgabe
        // -----------------
        System.out.println("\nIhr Fahrschein wird nun ausgegeben");  	

        for (int i = 0; i < 15; i++) //zählschleife:"i" wird auf "0" gesetzt, und solange i kleiner als 15 ist, wird es plus 1 gerechnet
        {
           System.out.print("="); // trennung mit zeitverzögerung von 150 millisekunden
           warte(150);}
        }
        public static void münzeAusgeben(int betrag, String einheit) {// Methode wird erstellt, Rückmünzen werden ausgegeben
    	System.out.printf("%d %s\n", betrag, einheit); 
    	
    	System.out.println("\nVergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen!\n");
	    System.out.println("Wir wünschen Ihnen eine gute Fahrt.");
	    System.out.printf("Bis zum nächsten mal!");
        System.out.println("\n\n"); // 1 Zeilen, jeweils oben und unten werden frei gelassen
        System.out.println("\n\n");
        }
               
    public static void warte(int millise) {	// Methode wird erstellt, mit einem try-catch Block, mit der Verzögerung				
		 try {
				Thread.sleep(millise);						
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	   
	}

    public static int fahrkartenZählen() { //Methode wird erstellt, worin die Menge der Karten gezählt wird die der Benutzer haben möchte
    	
    	Scanner tastatur = new Scanner(System.in);  

    	System.out.println("Wie viele Fahrkarten möchten Sie erwerben?:");
        System.out.println("====================================");
        anzahlkarten = tastatur.nextInt(); // einlesen der eingegebenen Menge des Benutzers
        
        while(anzahlkarten <1 || anzahlkarten >10) { // while schleife, solange die anzahl der karten kleiner als 1 ist oder größer als 10 wird schritt "return" folgen
            System.out.println("Wie viele Fahrkarten möchten Sie erwerben?:");
            System.out.println("====================================");
            anzahlkarten = tastatur.nextInt();
        }
        return anzahlkarten;
    }
}