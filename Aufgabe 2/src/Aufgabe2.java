
public class Aufgabe2 {

	public static void main(String[] args) {
	//%d steht f�r zahlen
	//%s steht f�r buchstaben 
		// positiv= rechtsb�ndig bei zahlen z.b. %7d
		// negativ= linksb�ndig (minus zeichen ) bei zahlen z.b %-7d
		// ich kann auch n als eine betsimmte zahl bestimmen z.b (int n = 1; oder 2; oder 3; oder oder oder )
		// bei einer zahl braucht man keine g�nsef�sschen
		// backslash n sprich \n hei�t abschlie�en
		
		int n = 1;
		System.out.printf("%d", n*0 );
		System.out.printf("%-4s", "!" );
		System.out.printf("%-20s" , "=" );
		System.out.printf("%s", "=");
		System.out.printf("%4d\n" , n ); // backslash n sprich \n hei�t abschlie�en
		
		System.out.printf("%d", n );
		System.out.printf("%-4s", "!");
		System.out.printf("%-2s" , "=");
		System.out.printf("%-18d" , n); // -18 weil ein leerzeichen bei = und 1 und die 1 dort auch ein platz weg nimmt sprich 20-2 =18
		System.out.printf("%s" , "=");
		System.out.printf("%4d\n" , n);
		
		System.out.printf("%d", n*2 );
		System.out.printf("%-4s", "!");
		System.out.printf("%-2s" , "=");
		System.out.printf("%-2d" , n);
		System.out.printf("%-2s", "*");
		System.out.printf("%-14d" , n*2);
		System.out.printf("%s" , "=");
		System.out.printf("%4d\n" , n*2);
	}

}
