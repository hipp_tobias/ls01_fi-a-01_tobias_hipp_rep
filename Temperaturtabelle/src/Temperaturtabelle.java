
public class Temperaturtabelle {

	public static void main(String[] args) {
		
		System.out.printf("%-11s" , "Fahrenheit");
		System.out.printf("%s" , "|");
		System.out.printf("%10s" , "Celsius");
		System.out.printf("\n%s","------------------------");
		System.out.printf("\n%-11s" , "-20");
		System.out.printf("%s" , "|");
		System.out.printf("%10.6s\n", "-28.89");
        System.out.printf("%-11s", "-10");
        System.out.printf("%s", "|");
        System.out.printf("%10.6s\n", "-23.3333");
        System.out.printf("%-11s", "+0");
        System.out.printf("%s", "|");
        System.out.printf("%10.6s\n", "-17.78");
        System.out.printf("%-11s", "+20");
        System.out.printf("%s", "|");
        System.out.printf("%10.5s\n", "-6.67");
        System.out.printf("%-11s", "+30");
        System.out.printf("%s", "|");
        System.out.printf("%10.5s\n", "-1.1111");
		
	}

}
